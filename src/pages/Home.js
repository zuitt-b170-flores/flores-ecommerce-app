
import { Fragment } from 'react';
import { Container } from 'react-bootstrap';
import Banner from '../components/Banner';
// import CourseCard from '../components/CourseCard';


/*
export default function Home() {
  return(
    <Fragment>
      <Banner />
      <Highlights />
    </Fragment>
  )
}*/

export default function Home(){

  const data = {
      title: "Home of the Big NBA Fans in Philippines!",
      content: "Your one stop for your favorite NBA accessories & apparels!"
  }


  return (
    <Fragment >
      <Container className="homeBanner" >
        <Banner data={data} />
      </Container >
    </Fragment>
  )
}

