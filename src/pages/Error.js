import React from 'react';
import { Link } from 'react-router-dom';

/*const Error = () => (
  <div>
    <h1>Page Not Found!</h1>
    <Link to="/">Go back to homepage</Link>
  </div>
);
*/


import Banner from '../components/Banner';
export default function Error (){
	const data = {
	title: "404 - Not Found",
	content: "The page you are looking for cannot be found",
	destination: "/",
	label: "Back Home"
	}
	return(
		<Banner data = {data}/>
	)
}
