import React from "react";
import {
  Box,
  Container,
  Row,
  Column,
  FooterLink,
  Heading,
  Footertext
} from "./FooterStyles";
  
const Footer = () => {
  return (
    <Box className="footer mt-5 p-2 mb-0 pt-5" fixed="bottom">
      <Container>
        <Row>
          <Row>
            <Heading>Partners</Heading>
            <FooterLink href="https://www.google.com" target="_blank">
              <i className="fab fa-google">Google</i>
            </FooterLink>
            <FooterLink href="https://nba.com" target="_blank">
              <i className="fab fa-nba">NBA</i>
            </FooterLink>
          </Row>
          <Row>
            <Heading>Follow Us</Heading>
            <FooterLink href="https://www.facebook.com" target="_blank">
              <i className="fab fa-facebook-f">
                <span>
                  Facebook
                </span>
              </i>
            </FooterLink>
            <FooterLink href="https://www.instagram.com" target="_blank">
              <i className="fab fa-instagram">
                <span>
                  Instagram
                </span>
              </i>
            </FooterLink>

            <FooterLink href="https://www.twitter.com" target="_blank">
              <i className="fab fa-twitter">
                <span>
                  Twitter
                </span>
              </i>
            </FooterLink>
            <FooterLink href="https://www.youtube.com" target="_blank">
              <i className="fab fa-youtube">
                <span>
                  Youtube
                </span>
              </i>
            </FooterLink>

          </Row>
        </Row>
            <Footertext>All Rights Reserved 2022</Footertext>
      </Container>
    </Box>
  );
};
export default Footer;
